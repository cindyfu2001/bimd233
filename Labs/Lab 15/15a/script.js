var stocks = [];

stocks.push({
  name: "Microsoft",
  market_cap: "$381.7 B",
  sales: "$86.8 B",
  profit: "$22.1 B",
  num_employees:128000
});

stocks.push({
  name: "Symetra Financial",
  market_cap: "$2.7 B",
  sales: "$2.2 B",
  profit: "$254.4 M",
  num_employees:1400
});

stocks.push({
  name: "Micron Technology",
  market_cap: "$37.6 B",
  sales: "16.4 B",
  profit: "$3.0 B",
  num_employees:30400
});

stocks.push({
  name: "F5 Networks",
  market_cap: "$9.5 B",
  sales: "$16.4 B",
  profit: "$3.0 B",
  num_employees:3834
});

stocks.push({
  name: "Expedia",
  market_cap: "$10.8 B",
  sales: "$5.8 B",
  profit: "$398.1 M",
  num_employees:18210
});

stocks.push({
  name: "Nautilus",
  market_cap: "$476 M",
  sales: "$274.4 M",
  profit: "$18.8 M",
  num_employees:340
});

stocks.push({
  name: "Heritage Financial",
  market_cap: "$531 M",
  sales: "$137.6 M",
  profit: "$21 M",
  num_employees:740
});

stocks.push({
  name: "Cascade Microtech",
  market_cap: "$239 M",
  sales: "$137.6 M",
  profit: "$21 M",
  num_employees:449
});

stocks.push({
  name: "Nike",
  market_cap: "$83.1 B",
  sales: "$27.8 B",
  profit: "$2.7 B",
  num_employees:56500
});

stocks.push({
  name: "Alaska Air Group",
  market_cap: "$61.6 B",
  sales: "$16.4 B",
  profit: "$2.1 B",
  num_employees:13952
});

// function debug(item) {
//   console.log(item.name, item.market_cap, item.sales, item.profit, item.num_employees);
// }


function mainFunction() {
  var nodeList = document.querySelectorAll("li.north-div");
  // console.log("Length Data:", scores.length, nodeList.length);
  nodeList.forEach(populateRow);
}

function populateRow(obj, i) {
  console.log("Obj:", obj, i);
  obj.innerHTML = buildRowHTML(stocks[i]);
  obj.id = stocks[i].letters;
}

function buildRowHTML(data) {
  let strHTML = '<div class="row">';
  strHTML += '<div class="col-sm college">' + data.name + "</div>";
  strHTML += '<div class="col-sm college">' + data.market_cap + "</div>";
  strHTML += '<div class="col-sm college">' + data.sales + "</div>";
  strHTML += '<div class="col-sm college">' + data.profit + "</div>";
  strHTML += '<div class="col-sm college">' + data.num_employees + "</div>";
  return strHTML;
}