var scores = [];

scores.push({
  school: "OREGON",
  conference: "6-2",
  overall: "9-2",
  last_game: "L 7-38 UTAH",
  letters: "or"
});

scores.push({
  school: "OREGON STATE",
  conference: "5-3",
  overall: "7-4",
  last_game: "W 24-10 ASU",
  letters: "osu"
});

scores.push({
  school: "WASHINGTON STATE",
  conference: "5-3",
  overall: "6-5",
  last_game: "W 44-18 ARIZ",
  letters: "wsu"
});

scores.push({
  school: "CALIFORNIA",
  conference: "3-4",
  overall: "4-6",
  last_game: "W 41-11 STAN",
  letters: "cal"
});

scores.push({
  school: "WASHINGTON",
  conference: "3-5",
  overall: "4-7",
  last_game: "L 17-20 COL",
  letters: "uw"
});

scores.push({
  school: "STANFORD",
  conference: "2-7",
  overall: "3-8",
  last_game: "L 11-41 CAL",
  letters: "su"
});

function mainFunction() {
  var nodeList = document.querySelectorAll("li.north-div");
  // console.log("Length Data:", scores.length, nodeList.length);
  nodeList.forEach(populateRow);
}

function populateRow(obj, i) {
  console.log("Obj:", obj, i);
  obj.innerHTML = buildRowHTML(scores[i]);
  obj.id = scores[i].letters;
}

function buildRowHTML(data) {
  let strHTML = '<div class="row">';
  strHTML += '<div class="col-sm college">' + data.school + "</div>";
  strHTML += '<div class="col-sm college">' + data.conference + "</div>";
  strHTML += '<div class="col-sm college">' + data.overall + "</div>";
  strHTML += '<div class="col-sm college">' + data.last_game + "</div>";
  return strHTML;
}