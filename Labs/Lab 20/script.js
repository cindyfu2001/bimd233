const baseURL = "https://api.coindesk.com/v1/bpi/currentprice/";
var myURL = baseURL + "USD.json";

var chartData = [];

var morris_chart_obj = {
  element: "price-chart",
  data: "",
  xkey: "time",
  ykeys: ["price"],
  labels: ["price"]
};

var priceChart = new Morris.Line(morris_chart_obj);

function getData() {
  $.ajax({
    url: myURL,
    success: function(data) {
      console.log(parseData(data));
      renderGraph(data);
      printPrice(data);
    }
  });
}

function renderGraph(data) {
  // console.log();
  plotMyData(parseData(data));
}

function printPrice() {}

function parseData(textPriceData) {
  let bcJSON = JSON.parse(textPriceData);
  // let currencyCode = $("span#currency-code").text();
  let currencyCode = "USD";
  let rateText = bcJSON.bpi[currencyCode].rate;
  rateText = rateText.replace(/,/g, "");
  return parseFloat(rateText);
} //Add way to input currency type 

function plotMyData(data) {
  updateData(data);
  priceChart.setData(chartData);
  // priceChart.redraw();
}

function updateData(price) {
  const maxChartValues = 5;
  var newEvent = {
    time: Date.now(),
    price: price
  };

  chartData.push(newEvent);
  if (chartData.length > maxChartValues) {
    chartData.shift();
  }
}


setInterval(getData, 3000);