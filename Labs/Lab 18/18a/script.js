$(document).ready(function () {
  $("li").css("id", "uw");

  $("ul").on("mouseover", "li", function () {
    console.log("mouseover: " + $(this).text());
    $(this).attr("id", "uw");
  });

  $("ul").on("mouseleave", "li", function () {
    console.log("mouseleave: " + $(this).text());
    $(this).attr("id", "uw-gold");
  });

  // reset button click
  $("button").on("click", function (e) {
    $("ul").empty();
  });

  //remove one to-do item
  $("ul").on("click", "li", function () {
    console.log("We clicked on the li: " + $(this).text());
    $(this).remove();
  });

  // keypress
  $("input").on("keypress", function (e) {
    const enterKey = 13;
    var state = "IDLE";
    var code = e.which;
    var char = String.fromCharCode(code);
    
    if (code === enterKey) {
      let inputVal = $("input").val();
      let insertHTML = "<li>" + inputVal + "</li>";
      console.log("key:" + code + "\tchar:" + char + "\tdata:" + inputVal);
      $("ul").append(insertHTML);
      $("input").val("");
    }
  });
});