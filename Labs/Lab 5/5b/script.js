// Var margin= 20;
// Var x1 = 200;
// Var x2 = 400;
// Var y1 = 200;
// Var y2 = 400;
// Var w = 200;
// Var h = 600;


var canvas=document.getElementById("myCanvas");
var context=canvas.getContext("2d"); 
var margin = 20;
var x1 = 200, x2 = 400, y1 = 200, y2 = 400, w = 600, h = 600;

context.moveTo(margin, x1);
context.lineTo(w-margin, x1);
context.lineWidth=3;
context.stroke();

context.moveTo(margin, x2);
context.lineTo(w-margin, x2);
context.lineWidth=3;
context.stroke();

context.moveTo(x1, margin);
context.lineTo(x1, w-margin);
context.lineWidth=3;
context.stroke();

context.moveTo(x2, margin);
context.lineTo(x2, w-margin);
context.lineWidth=3;
context.stroke();

context.moveTo(x1-margin, x1*0.5)
var radius=80, angle=2*Math.PI;
context.arc(0.5*x1,0.5*x1, radius, 0, angle);
context.stroke();

context.moveTo(w-margin, x1*2.5)
var radius=80, angle=2*Math.PI;
context.arc(2.5*x1, 2.5*x1, radius, 0, angle);
context.stroke();

context.moveTo(x1+margin, x1+margin)
context.lineTo(x2-margin, x2-margin);
context.lineWidth=3;
context.stroke();

context.moveTo(x2-margin, x1+margin)
context.lineTo(x1+margin, x2-margin)
context.lineWidth=3;
context.stroke();