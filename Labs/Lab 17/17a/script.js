$(document).ready(function () {
  // add a 5px red dashed boarder around the panel div
  // provide a 10 pixel padding around all of the divs
  $(".card").css("border", "5px dashed red");
  $(".container").css("padding", "10px");
  $("div").css("padding", "3px");
  // set all div's padding to 3px
  $(".card").css("background", "black");
  // set all divs in the panel to background gray

  // set all divs of class cat to green
  $(".cat").css("color", "green");
  $(".cat").css("background-color", "grey");
  // set all divs of class dog to red
  $(".dog").css("color", "red");
  $(".dog").css("background", "yellow");
  // set the id of lab to a 1px solid yellow border
  $("#lab").css("border", "1px solid yellow");
  $("#lab").css("background", "grey");
  // set the last div with class dog to background yellow
  $("#calico").css("width", "50%");
  $("#calico").css("background-color", "green");
  $("#calico").css("color", "white");
  // set the calico cat's width to 50%,
  // background to green and color to white
});