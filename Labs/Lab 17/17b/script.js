// $(document).ready(function () {
  // --------- jQuery Data Section ---------
  var book1 = {
    title: "A Thousand Brains: A New Theory of Intelligence",
    author: "Jeff Hawkins",
    image: "https://media.gatesnotes.com/-/media/Images/Books/A-Thousand-Brains/a_thousand_brains_20211116_article-hero_1200x564.ashx"
  };
  var book2 = {
    title: "The Code Breaker: Jennifer Doudna, Gene Editing, and the Future of the Human Race",
    author: "Walter Isaacson",
    image: "https://media.gatesnotes.com/-/media/Images/Books/The-Code-Breaker/codebreaker_2021_20211116_article-hero_1200x564.ashx"
  };
  var book3 = {
    title: "Klara and the Sun",
    author: "Kazuo Ishiguro",
    image: "https://media.gatesnotes.com/-/media/Images/Books/Klara-and-the-Sun/klara_and_the_sun_20210827_article-hero_1200x564.ashx"
  };
  var book4 = {
    title: "Hamnet",
    author: "Maggie O’Farrell",
    image: "https://media.gatesnotes.com/-/media/Images/Books/Hamnet/hamnet_20210827_article-hero_1200x564.ashx"
  };
  var book5 = {
    title: "Project Hail Mary",
    author: "Andy Weir",
    image: "https://media.gatesnotes.com/-/media/Images/Books/Project-Hail-Mary/project_hail_mary_20210827_article-hero_1200x564.ashx"
  };

  var books = new Array;
  books.push(book1);
  books.push(book2);
  books.push(book3);
  books.push(book4);
  books.push(book5);

  var img_ref = {
    url: 'https://media.gatesnotes.com/-/media/Images/Articles/About-Bill-Gates/Holiday-Books-2021/holiday_books_2021_20210827_blog-roll_800x494.ashx',
    src: 'https://gatesnot.es/3lk3sWK',
    alt: 'Bill Gates',
    height: 100, // orig 401 by 4:1
    width: 133 // orig 534 by 4:1  
  };

  var reference = {
    url: 'https://www.gatesnotes.com/About-Bill-Gates/Holiday-Books-2021',
    src: 'https://gatesnot.es/3p8dsDv',
    alt: 'Gates Books',
    text: '5 books I loved reading this year'
  }
  // --------- jQuery Data Section ---------


  // --------- jQuery Code Section ---------

  // apply bootstrap list group classes
  $('ol').addClass("list-group");
  $('li').addClass("list-group-item");

  $('li').each(function (i) {
    $(this).html("<i>\"" + books[i].title + "\"</i> by " + books[i].author +'"<img src="' + books[i].image + '" width="500" >'); 
  });

$('<img />')
            .attr('src', 'https://media.gatesnotes.com/-/media/Images/Articles/About-Bill-Gates/Holiday-Books-2021/holiday_books_2021_20210827_blog-roll_800x494.ashx')
            .width('133px')
            .height('100px')
            .appendTo($('#bg-pic'));

  $('li').each(function (i) {
    // add your row striping code here
  });
  // --------- jQuery Code Section ---------
// });