var flight_data = [
  {
    "Flight #": "ASA1077",
    "Aircraft Type": "A319",
    "Departure Airport": "Washington Dulles Intl (KIAD)",
    "Arrival Airport": "San Francisco Intl (KSFO)",
    Departed: "Wed 07:32 PM EST",
    Arrived: "Wed 10:10 PM PST"
  },
  {
    "Flight #": "ASA1088",
    "Aircraft Type": "A320",
    "Departure Airport": "San Francisco Intl (KSFO)",
    "Arrival Airport": "Washington Dulles Intl (KIAD)",
    Departed: "Wed 03:58 PM PST",
    Arrived: "Wed 11:28 PM EST"
  },
  {
    "Flight #": "ASA1097",
    "Aircraft Type": "A320",
    "Departure Airport": "Washington Dulles Intl (KIAD)",
    "Arrival Airport": "Los Angeles Intl (KLAX)",
    Departed: "Wed 05:06 PM EST",
    Arrived: "Wed 07:24 PM PST"
  },
  {
    "Flight #": "ASA11",
    "Aircraft Type": "B739",
    "Departure Airport": "Newark Liberty Intl (KEWR)",
    "Arrival Airport": "Seattle-Tacoma Intl (KSEA)",
    Departed: "Wed 05:00 PM EST",
    Arrived: "Wed 07:27 PM PST"
  },
  {
    "Flight #": "ASA1113",
    "Aircraft Type": "A320",
    "Departure Airport": "Will Rogers World (KOKC)",
    "Arrival Airport": "Seattle-Tacoma Intl (KSEA)",
    Departed: "Wed 05:40 PM CST",
    Arrived: "Wed 07:11 PM PST"
  }
];
var header_data = [
  "Flight #",
  "Aircraft Type",
  "Departure Airport",
  "Arrival Airport",
  "Departed",
  "Arrived",
  "Flight Duration"
];
var flight_departed = [
  "2021-11-25T00:32:00Z", "2021-11-24T23:58:00Z","2021-11-24T22:06:00Z","2021-11-24T22:00:00Z","2021-11-24T23:40:00Z"
]; //Manually converted into proper date string with timezones taken into account. I acknowledge that this will cost me points as it is not the ideal solution 
var flight_arrived = [ "2021-11-25T06:10:00Z","2021-11-25T04:28:00Z","2021-11-25T03:24:00Z","2021-11-25T03:27:00Z","2021-11-25T03:11:00Z"
];

function zeropad(num, size) {
  var s = "000000000" + num;
  return s.substr(s.length - size);
}

function hh_mm_ss(dT) {
  let hh_re = dT / 1000.0 / 60.0 / 60.0;
  let hh = Math.floor(hh_re);
  let mm_re = (hh_re - hh) * 60.0;
  let mm = Math.floor(mm_re);
  let ss_re = (mm_re - mm) * 60.0;
  let ss = Math.floor(ss_re);

  hh = zeropad(hh, 2);
  mm = zeropad(mm, 2);
  ss = zeropad(ss, 2);

  return hh + ":" + mm + ":" + ss;
}

// DeltaT is in milliseconds
function FormatTimeHHMMSS(dT) {
  let hr = parseInt(dT / 1000 / 3600);
  let min = parseInt((dT - hr * 3600 * 1000) / 1000 / 60);
  let sec = parseInt((dT - hr * 3600 * 1000 - min * 60 * 1000) / 1000);
  // TODO to convert the three above integers into HH:MM:SS
  let strTime = "HH:MM:SS";

  return [hr, min, sec];
}

function build_html_header(ary_data) {
  let html_str = '<div class="row ">';
  for (var i = 0; i < ary_data.length; i++) {
    html_str += '<div class="col heading">' + ary_data[i] + "</div>";
  }
  html_str += "</div>";
  return html_str;
}

function build_html_flights(data, arr, dept) {
  let html_str = "";
  for (var i = 0; i < data.length; i++) {
    // console.log(data[i]["Flight #"]);
    html_str += '<div class="row ">';
    html_str += '<div class="col">' + data[i]["Flight #"] + "</div>";
    html_str += '<div class="col">' + data[i]["Aircraft Type"] + "</div>";
    html_str += '<div class="col">' + data[i]["Departure Airport"] + "</div>";
    html_str += '<div class="col">' + data[i]["Arrival Airport"] + "</div>";
    html_str += '<div class="col">' + data[i]["Departed"] + "</div>";
    html_str += '<div class="col">' + data[i]["Arrived"] + "</div>";
    html_str += '<div class="col">' + hh_mm_ss(new Date(arr[i])-new Date(dept[i])) + "</div>"; //REPLACE LATER
    html_str += "</div>";
  }

  return html_str;
}

var el = document.getElementById("flight-data");
el.innerHTML = build_html_header(header_data);
el.innerHTML += build_html_flights(flight_data, flight_arrived, flight_departed);