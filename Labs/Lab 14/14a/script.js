var wx_data = [
  { day: "fri", hi: 82, lo: 55 },
  { day: "sat", hi: 75, lo: 52 },
  { day: "sun", hi: 69, lo: 52 },
  { day: "mon", hi: 69, lo: 48 },
  { day: "tue", hi: 68, lo: 51 }
];
var high_temp = Math.max.apply(Math,wx_data.map(function(data){return data.hi;}))

var low_temp = Math.min.apply(Math,wx_data.map(function(data){return data.lo;}))

console.log('5 day high :' + high_temp);

function mainFunction() {
  var nodeList = document.querySelectorAll("li.north-div");
  // console.log("Length Data:", wd_data.length, nodeList.length);
  nodeList.forEach(populateRow);
  document.getElementById("wx_hi").innerHTML = "Avg high: " + hi_temp_avg(wx_data).toFixed(2);
  document.getElementById("wx_lo").innerHTML = "Avg low: " + lo_temp_avg(wx_data).toFixed(2);
  document.getElementById("wx_avg").innerHTML = "Avg temp: " + ((lo_temp_avg(wx_data)+hi_temp_avg(wx_data))/2).toFixed(2);
}

const hi_temp_avg = (arr) => {
   const { length } = arr;
   return arr.reduce((acc, val) => {
      return acc + (val.hi/length);
   }, 0);
};

const lo_temp_avg = (arr) => {
   const { length } = arr;
   return arr.reduce((acc, val) => {
      return acc + (val.lo/length);
   }, 0);
};

function populateRow(obj, i) {
  // console.log("Obj:", obj, i);
  obj.innerHTML = buildRowHTML(wx_data[i]);
  obj.id = wx_data[i].letters;
}

function buildRowHTML(data) {
  let strHTML = '<div class="row">';
  strHTML += '<div class="col-sm college">' + data.day + "</div>";
  strHTML += '<div class="col-sm college">' + data.hi + "</div>";
  strHTML += '<div class="col-sm college">' + data.lo + "</div>";
  return strHTML;
}

function printAllRows(data) {
  let el = document.querySelectorAll("li.north-div");
  nodeList.forEach(populateRow);
  buildRowHTML(el);
}

mainFunction();