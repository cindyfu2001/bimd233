//Classifying all car variables 
var carMake = ['Honda', 'Toyota', 'Acura', 'Ford', 'Hyundai'];
var carModel = ['Accord', 'RAV4', 'TLX', 'Mustang', 'Elantra'];
var carYear = 2021;
var carPrice = ['$24,970','$26,350', '$37,300','$27,205','19,650'];
var carMPG = ['30/38', '28/35','22/31','21/32','33/41'];


//Need function to call in each row

//Need function to change carVALUE + 1 by each row 


function printAllRows(data) {
    let el = document.getElementById("data");
    renderRow(el);
}

//This function is going to render HTML
function renderRow(el) {
  for (var i = 0; i <= 4; i++) {
  el.innerHTML += "<div class='row'>";
  el.innerHTML +=   "<div class='col'>" + carMake[i]+ "</div>";
  el.innerHTML +=   "<div class='col'>" + carModel[i] + "</div>";
  el.innerHTML +=   "<div class='col'>"+ carYear + "</div>";
  el.innerHTML +=   "<div class='col'>" + carPrice[i] + "</div>";
  el.innerHTML +=   "<div class='col'>" + carMPG[i] + "</div>";
  el.innerHTML += "</div>"
  }
}


printAllRows();