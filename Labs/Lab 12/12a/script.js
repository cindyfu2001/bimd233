//Input: radius
//Output: Area, circumfrence, diameter
var radii = [];
const NUM_RADII = 5;

function calcCircleGeometries(radius) {
  const pi = Math.PI;
  var area = pi * radius * radius;
  var circumference = 2 * pi * radius;
  var diameter = 2 * radius;
  var gemoetries = [area, circumference, diameter];
  return gemoetries;
}

function printAllRows(data) {
  const N = data.length;
  for (var i = 0; i < NUM_RADII; i++) {
    let r = data[i];
    let el = document.getElementById("data");
    renderRow(el, r, calcCircleGeometries(r));
  }
}

// console.log(r, calcCircleGeometries(r));

//This function is going to render HTML
function renderRow(el, r, geoms) {
  el.innerHTML += "<div class='row'>";
  el.innerHTML +=   "<div class='col'>" + r.toFixed(1) + "</div>";
  el.innerHTML +=   "<div class='col'>" + geoms[0].toFixed(3) + "</div>";
  el.innerHTML +=   "<div class='col'>"+ geoms[1].toFixed(2) + "</div>";
  el.innerHTML +=   "<div class='col'>" + geoms[2].toFixed(2) + "</div>";
  el.innerHTML += "</div>"
}

//For loop
for (var i = 0; i < NUM_RADII; i++) {
  radii[i] = Math.random() * 100;
  let r = radii[i]; //Let, temporary variable
  // console.log(i, r, calcCircleGeometries(r));
}

printAllRows(radii);

// Where code starts running
// Basic test of our code thusfar
// console.log("R1 "+ r1 + " Circle Geometries: " + calcCircleGeometries(r1));
// console.log(r2, calcCircleGeometries(r2));
// console.log(r3, calcCircleGeometries(r3));